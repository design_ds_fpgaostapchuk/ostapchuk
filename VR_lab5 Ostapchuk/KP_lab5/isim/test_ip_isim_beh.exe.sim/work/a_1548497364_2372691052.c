/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/XI_LABS/KP_lab5/test_ip.vhd";
extern char *IEEE_P_3499444699;
extern char *IEEE_P_3620187407;

char *ieee_p_3499444699_sub_2213602152_3536714472(char *, char *, int , int );
int ieee_p_3620187407_sub_514432868_3965413181(char *, char *, char *);


static void work_a_1548497364_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;

LAB0:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(78, ng0);
    t2 = (t0 + 3584);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(79, ng0);
    t7 = (10 * 1000LL);
    t2 = (t0 + 2760);
    xsi_process_wait(t2, t7);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(80, ng0);
    t2 = (t0 + 3584);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(81, ng0);
    t7 = (10 * 1000LL);
    t2 = (t0 + 2760);
    xsi_process_wait(t2, t7);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_1548497364_2372691052_p_1(char *t0)
{
    char t8[16];
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    int64 t6;
    char *t7;
    int t9;
    int t10;
    char *t11;
    unsigned int t12;
    unsigned char t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    t1 = (t0 + 3200U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(86, ng0);
    t2 = (t0 + 5548);
    *((int *)t2) = 0;
    t3 = (t0 + 5552);
    *((int *)t3) = 3;
    t4 = 0;
    t5 = 3;

LAB4:    if (t4 <= t5)
        goto LAB5;

LAB7:    xsi_set_current_line(91, ng0);
    t2 = (t0 + 5556);
    *((int *)t2) = 0;
    t3 = (t0 + 5560);
    *((int *)t3) = 5;
    t4 = 0;
    t5 = 5;

LAB19:    if (t4 <= t5)
        goto LAB20;

LAB22:    xsi_set_current_line(95, ng0);
    t2 = (t0 + 5572);
    *((int *)t2) = 0;
    t3 = (t0 + 5576);
    *((int *)t3) = 3;
    t4 = 0;
    t5 = 3;

LAB28:    if (t4 <= t5)
        goto LAB29;

LAB31:    xsi_set_current_line(100, ng0);
    t2 = (t0 + 5580);
    *((int *)t2) = 0;
    t3 = (t0 + 5584);
    *((int *)t3) = 5;
    t4 = 0;
    t5 = 5;

LAB43:    if (t4 <= t5)
        goto LAB44;

LAB46:    goto LAB2;

LAB5:    xsi_set_current_line(87, ng0);
    t6 = (10 * 1000LL);
    t7 = (t0 + 3008);
    xsi_process_wait(t7, t6);

LAB10:    *((char **)t1) = &&LAB11;

LAB1:    return;
LAB6:    t2 = (t0 + 5548);
    t4 = *((int *)t2);
    t3 = (t0 + 5552);
    t5 = *((int *)t3);
    if (t4 == t5)
        goto LAB7;

LAB18:    t9 = (t4 + 1);
    t4 = t9;
    t7 = (t0 + 5548);
    *((int *)t7) = t4;
    goto LAB4;

LAB8:    xsi_set_current_line(88, ng0);
    t2 = (t0 + 1192U);
    t3 = *((char **)t2);
    t2 = (t0 + 5476U);
    t9 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t2);
    t10 = (t9 + 1);
    t7 = ieee_p_3499444699_sub_2213602152_3536714472(IEEE_P_3499444699, t8, t10, 8);
    t11 = (t8 + 12U);
    t12 = *((unsigned int *)t11);
    t12 = (t12 * 1U);
    t13 = (8U != t12);
    if (t13 == 1)
        goto LAB12;

LAB13:    t14 = (t0 + 3648);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    memcpy(t18, t7, 8U);
    xsi_driver_first_trans_fast(t14);
    xsi_set_current_line(89, ng0);
    t6 = (10 * 1000LL);
    t2 = (t0 + 3008);
    xsi_process_wait(t2, t6);

LAB16:    *((char **)t1) = &&LAB17;
    goto LAB1;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

LAB12:    xsi_size_not_matching(8U, t12, 0);
    goto LAB13;

LAB14:    goto LAB6;

LAB15:    goto LAB14;

LAB17:    goto LAB15;

LAB20:    xsi_set_current_line(92, ng0);
    t7 = (t0 + 5564);
    t14 = (t0 + 3648);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    memcpy(t18, t7, 8U);
    xsi_driver_first_trans_fast(t14);
    xsi_set_current_line(93, ng0);
    t6 = (20 * 1000LL);
    t2 = (t0 + 3008);
    xsi_process_wait(t2, t6);

LAB25:    *((char **)t1) = &&LAB26;
    goto LAB1;

LAB21:    t2 = (t0 + 5556);
    t4 = *((int *)t2);
    t3 = (t0 + 5560);
    t5 = *((int *)t3);
    if (t4 == t5)
        goto LAB22;

LAB27:    t9 = (t4 + 1);
    t4 = t9;
    t7 = (t0 + 5556);
    *((int *)t7) = t4;
    goto LAB19;

LAB23:    goto LAB21;

LAB24:    goto LAB23;

LAB26:    goto LAB24;

LAB29:    xsi_set_current_line(96, ng0);
    t6 = (10 * 1000LL);
    t7 = (t0 + 3008);
    xsi_process_wait(t7, t6);

LAB34:    *((char **)t1) = &&LAB35;
    goto LAB1;

LAB30:    t2 = (t0 + 5572);
    t4 = *((int *)t2);
    t3 = (t0 + 5576);
    t5 = *((int *)t3);
    if (t4 == t5)
        goto LAB31;

LAB42:    t9 = (t4 + 1);
    t4 = t9;
    t7 = (t0 + 5572);
    *((int *)t7) = t4;
    goto LAB28;

LAB32:    xsi_set_current_line(97, ng0);
    t2 = (t0 + 1192U);
    t3 = *((char **)t2);
    t2 = (t0 + 5476U);
    t9 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t2);
    t10 = (t9 + 2);
    t7 = ieee_p_3499444699_sub_2213602152_3536714472(IEEE_P_3499444699, t8, t10, 8);
    t11 = (t8 + 12U);
    t12 = *((unsigned int *)t11);
    t12 = (t12 * 1U);
    t13 = (8U != t12);
    if (t13 == 1)
        goto LAB36;

LAB37:    t14 = (t0 + 3648);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    memcpy(t18, t7, 8U);
    xsi_driver_first_trans_fast(t14);
    xsi_set_current_line(98, ng0);
    t6 = (10 * 1000LL);
    t2 = (t0 + 3008);
    xsi_process_wait(t2, t6);

LAB40:    *((char **)t1) = &&LAB41;
    goto LAB1;

LAB33:    goto LAB32;

LAB35:    goto LAB33;

LAB36:    xsi_size_not_matching(8U, t12, 0);
    goto LAB37;

LAB38:    goto LAB30;

LAB39:    goto LAB38;

LAB41:    goto LAB39;

LAB44:    xsi_set_current_line(101, ng0);
    t7 = (t0 + 5588);
    t14 = (t0 + 3648);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    memcpy(t18, t7, 8U);
    xsi_driver_first_trans_fast(t14);
    xsi_set_current_line(102, ng0);
    t6 = (20 * 1000LL);
    t2 = (t0 + 3008);
    xsi_process_wait(t2, t6);

LAB49:    *((char **)t1) = &&LAB50;
    goto LAB1;

LAB45:    t2 = (t0 + 5580);
    t4 = *((int *)t2);
    t3 = (t0 + 5584);
    t5 = *((int *)t3);
    if (t4 == t5)
        goto LAB46;

LAB51:    t9 = (t4 + 1);
    t4 = t9;
    t7 = (t0 + 5580);
    *((int *)t7) = t4;
    goto LAB43;

LAB47:    goto LAB45;

LAB48:    goto LAB47;

LAB50:    goto LAB48;

}


extern void work_a_1548497364_2372691052_init()
{
	static char *pe[] = {(void *)work_a_1548497364_2372691052_p_0,(void *)work_a_1548497364_2372691052_p_1};
	xsi_register_didat("work_a_1548497364_2372691052", "isim/test_ip_isim_beh.exe.sim/work/a_1548497364_2372691052.didat");
	xsi_register_executes(pe);
}
