-- Vhdl test bench created from schematic D:\XI_LABS\KP_lab4\KP_unConv_sh.sch - Mon Dec 11 16:44:22 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY KP_unConv_sh_KP_unConv_sh_sch_tb IS
END KP_unConv_sh_KP_unConv_sh_sch_tb;
ARCHITECTURE behavioral OF KP_unConv_sh_KP_unConv_sh_sch_tb IS 

   COMPONENT KP_unConv_sh
   PORT( KP_A	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_B	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_D	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_CO	:	OUT	STD_LOGIC; 
          KP_T	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_Less_R	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_Great_R	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_CO_Res	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL KP_A	:	STD_LOGIC_VECTOR (15 DOWNTO 0):=x"0001";
   SIGNAL KP_B	:	STD_LOGIC_VECTOR (15 DOWNTO 0):=x"0002";
   SIGNAL KP_D	:	STD_LOGIC_VECTOR (15 DOWNTO 0):=x"0003";
   SIGNAL KP_CO	:	STD_LOGIC;
   SIGNAL KP_T	:	STD_LOGIC_VECTOR (15 DOWNTO 0):=x"0004";
   SIGNAL KP_Less_R	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL KP_Great_R	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL KP_CO_Res	:	STD_LOGIC;

BEGIN

   UUT: KP_unConv_sh PORT MAP(
		KP_A => KP_A, 
		KP_B => KP_B, 
		KP_D => KP_D, 
		KP_CO => KP_CO, 
		KP_T => KP_T, 
		KP_Less_R => KP_Less_R, 
		KP_Great_R => KP_Great_R, 
		KP_CO_Res => KP_CO_Res
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
		KP_A <= x"0234" after 10 ns;
		KP_B <= x"0E1C" after 10 ns;
		KP_D <= x"6EC3" after 10 ns;
		KP_T <= x"14FA" after 10 ns;
		WAIT for 10 ns; -- will wait forever
		KP_A <= x"D3E5" after 10 ns;
		KP_B <= x"98C0" after 10 ns;
		KP_D <= x"19F3" after 10 ns;
		KP_T <= x"56E7" after 10 ns;
		WAIT for 10 ns; 
		KP_A <= x"1234" after 10 ns;
		KP_B <= x"4321" after 10 ns;
		KP_D <= x"5678" after 10 ns;
		KP_T <= x"8765" after 10 ns;
		WAIT for 10 ns; 
		KP_A <= x"1ECA" after 10 ns;
		KP_B <= x"23DA" after 10 ns;
		KP_D <= x"45CE" after 10 ns;
		KP_T <= x"90AB" after 10 ns;
		WAIT for 10 ns; 
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
