-- Vhdl test bench created from schematic D:\XI_LABS\KP_lab4\test_sh.sch - Mon Dec 11 16:02:51 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY test_sh_test_sh_sch_tb IS
END test_sh_test_sh_sch_tb;
ARCHITECTURE behavioral OF test_sh_test_sh_sch_tb IS 

   COMPONENT test_sh
   PORT( KP_A	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_B	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          KP_C	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0));
   END COMPONENT;

   SIGNAL KP_A	:	STD_LOGIC_VECTOR (15 DOWNTO 0):=x"1234";
   SIGNAL KP_B	:	STD_LOGIC_VECTOR (15 DOWNTO 0):=x"4321";
   SIGNAL KP_C	:	STD_LOGIC_VECTOR (31 DOWNTO 0);

BEGIN

   UUT: test_sh PORT MAP(
		KP_A => KP_A, 
		KP_B => KP_B, 
		KP_C => KP_C
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
		KP_A <= x"FFFF";
		KP_B <= x"1111";
      WAIT for 10 ns; -- will wait forever
		KP_A <= x"0000";
		KP_B <= x"1111";
      WAIT for 10 ns;
		KP_A <= x"4A5C";
		KP_B <= x"9E04";
      WAIT for 10 ns; 
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
